all:
	g++ -Wall -Wextra -pedantic -ggdb -o ant.exe ant.cpp funcs.cpp -lgdi32
release:
	g++ -Wall -Wextra -pedantic -O5 -o ant.exe ant.cpp funcs.cpp -lgdi32
clean:
	rm *.exe
