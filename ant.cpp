#include "CImg.h"
#include "defs.hpp"
#include "funcs.hpp"

using namespace cimg_library;

void display(std::vector<CHolder> rules){
  CImg<unsigned char> img(maxX,maxY,1,3,0);
  CImgDisplay disp(img,"Ant");
  img.fill(192);
  int x = maxX/2;
  int y = maxY/2;
  int direction = UP;

  CHolder rule;
  while(!disp.is_closed() && !disp.is_keyQ() && !disp.is_keyESC()) {
     unsigned char val_red   = img(x, y, 0);
	 unsigned char val_green = img(x, y, 1);
	 unsigned char val_blue = img(x, y, 2);
     unsigned int i;
	 CHolder newRule;
     for(i = 0; i < rules.size(); i++){
       rule = rules.at(i);
	   if (val_red == rule.getColorR() && val_green == rule.getColorG() && val_blue == rule.getColorB()) {
		   if (i == rules.size() - 1) {
			   newRule = rules.at(0);
		   }
		   else
			   newRule = rules.at(i + 1);
		   break;
	   }
     }

     direction = newDirection(rule.getRotation(), direction);
     unsigned char color[] = {newRule.getColorR() , newRule.getColorG(), newRule.getColorB()};
     img.draw_point(x,y,color);

     newCoordinates(direction,x,y);
     img.display(disp.wait(1));
  }
}


int main() {
  std::vector <CHolder>rules = initRules();
  display(rules);
  
  return 0;
}


