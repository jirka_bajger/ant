#ifndef	_DEFS_HPP_
#define	_DEFS_HPP_ 1

#include <iostream>
#include <algorithm>
#include <vector>
#include <iterator>
#include "holder.hpp"

//turns
#define R 1
#define L 0
//directions
#define UP 0
#define RIGHT 1
#define DOWN 2
#define LEFT 3

const int maxX(200), maxY(200);
#endif
