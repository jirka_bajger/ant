#include "defs.hpp"
/*turn L -> 0, R -> 1*/
int newDirection(int turn, int dir){
  int retvalue = turn == R? dir + 1:dir - 1;
  if(retvalue<0) retvalue = LEFT;

  return retvalue%4;
}

void newCoordinates(int direction, int &x, int &y){
  switch (direction){
  case UP:
    y -= 1;
    break;
  case RIGHT:
    x += 1;
    break;
  case DOWN:
    y +=1;
    break;
  case LEFT:
    x -= 1;
    break;
  }
  if(x > maxX) x = 0;
  if(y > maxY) y = 0;
  if(y < 0) y = maxY - 1;
  if(x < 0) x = maxX - 1;
}

std::vector<CHolder> initRules(){
  std::vector <CHolder>rules;

  CHolder h1(0, 0, 0, R);
  CHolder h2(64, 0, 64, L);
  CHolder h3(0, 128, 0, L);
  CHolder h4(0, 0, 192, R);
  CHolder h5(255, 100, 50, L);
  
  rules.push_back(h1);
  rules.push_back(h2);
  rules.push_back(h3);
  rules.push_back(h4);
  rules.push_back(h5);
  /*
  int turn;
  for(int i = 0; i < 255; i++){
    turn = i%2? L:R;
    CHolder holder(255 - i,turn);
    rules.push_back(holder);
  }
  */
  return rules;
}
