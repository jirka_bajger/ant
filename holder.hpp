class CHolder
{
  unsigned char colorR;
  unsigned char colorG;
  unsigned char colorB;
  int rotation;
public:
  CHolder(){
	  colorR = 0;
	  colorG = 0;
	  colorB = 0;
	  rotation = 0;
  };
  CHolder(const unsigned char colR, const unsigned char colG, const unsigned char colB, const int rot){
    colorR = colR;
	colorG = colG;
	colorB = colB;
    rotation = rot;
  };
  unsigned char getColorR() { return colorR; };
  unsigned char getColorG() { return colorG; };
  unsigned char getColorB() { return colorB; };
  int getRotation(){return rotation;};
};
