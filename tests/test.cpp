#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "../defs.hpp"
#include "../funcs.cpp"

TEST_CASE( "newDirection", "[newDirection]" ) {
  REQUIRE( newDirection(R,UP) == RIGHT );
}
